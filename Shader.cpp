#include "pch.h"
#include "Shader.h"

Shader::Shader()
{
	m_prog = 0;
	m_vert = 0;
	m_frag = 0;
	m_loaded = false;
}

GLuint Shader::_loadShader(const std::string &source, GLenum type)
{
	GLuint shader = glCreateShader(type);
	const char *str = source.c_str();
	glShaderSource(shader, 1, &str, 0);
	glCompileShader(shader);
	return _checkCompilation(shader) ? shader : 0;
}

GLuint Shader::_link()
{
	GLuint prog = glCreateProgram();
	glAttachShader(prog, m_vert);
	glAttachShader(prog, m_frag);
	glBindAttribLocation(prog, ATT_VIGNETTE, "attVignette");
	glBindAttribLocation(prog, ATT_TEXR, "attTexR");
	glBindAttribLocation(prog, ATT_TEXG, "attTexG");
	glBindAttribLocation(prog, ATT_TEXB, "attTexB");
	glLinkProgram(prog);
	if(!_checkLinking(prog)) return 0;
	
//    glValidateProgram(prog);
//    if(!_checkValidation(prog)) return 0;
	
	return prog;
}

void Shader::_destroy()
{
	glDetachShader(m_prog, m_vert);
	glDetachShader(m_prog, m_frag);
	glDeleteProgram(m_prog);
	m_locations.clear();
	m_loaded = false;
}

bool Shader::_checkCompilation(GLuint shader)
{
	GLint r;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &r);
	if(!r)
	{
		GLchar msg[1024];
		glGetShaderInfoLog(shader, sizeof(msg), 0, msg);
		printf("Compiling shader\nfailed: %s\n", msg);
		return 0;
	}
	return 1;
}

bool Shader::_checkLinking(GLuint prog)
{
	GLint r;
	glGetProgramiv(prog, GL_LINK_STATUS, &r);
	if(!r)
	{
		GLchar msg[1024];
		glGetProgramInfoLog(prog, sizeof(msg), 0, msg);
		printf("Linking shaders failed: %s\n", msg);
		return 0;
	}
	return 1;
}

bool Shader::_checkValidation(GLuint prog)
{
	GLint r;
	glGetProgramiv(prog, GL_VALIDATE_STATUS, &r);
	if(!r)
	{
		GLchar msg[1024];
		glGetProgramInfoLog(prog, sizeof(msg), 0, msg);
		printf("%s\n", msg);
		return 0;
	}
	return 1;
}

bool Shader::load(const std::string &vertex, const std::string &fragment)
{
	m_loaded = false;
	if(!(m_vert = _loadShader(vertex, GL_VERTEX_SHADER))) return 0;
	if(!(m_frag = _loadShader(fragment, GL_FRAGMENT_SHADER))) return 0;
	if(!(m_prog = _link()))
	{
		_destroy();
		printf("Error in shader %s/%s\n", vertex.c_str(), fragment.c_str());
		return 0;
	}
	m_loaded = 1;
	return 1;
}

void Shader::unload()
{
	_destroy();
}

void Shader::use()
{
	glUseProgram(m_prog);
}

bool Shader::isLoaded()
{
	return m_loaded;
}

GLuint Shader::getUniformLocation(const std::string &name)
{
	if(m_locations.count(name) == 0)
		m_locations[name] = glGetUniformLocation(m_prog, name.c_str());
	return m_locations[name];
}
