#include "pch.h"
#include "ShaderManager.h"
#include "Primitive.h"

#define RT_PARALLEL

struct rtObject
{
	virtual struct rtIntersection Intersect(const struct rtRay& r) const = 0;
	virtual void CompleteIntersection(const struct rtRay& ray, struct rtIntersection& is) const = 0;
};

struct rtRay
{
	glm::vec3 pos;
	glm::vec3 dir;
	rtRay() = default;
	rtRay(glm::vec3 p, glm::vec3 d) : pos(p), dir(d) { }
};

struct rtIntersection
{
	float t;
	const rtObject* obj;
	rtRay r;
	glm::vec3 p, n;
	bool front;
	bool valid;
	rtIntersection()
	{
		valid = false;
	}
	rtIntersection(float ft, const rtObject* object, const rtRay& ray, boolean front)
	{
		t = ft;
		obj = object;
		r = ray;
		valid = true;
	}
	static rtIntersection Find(const rtRay& ray, 
		const std::vector<const rtObject*>& objects, 
		const std::vector<const rtObject*>& excluded)
	{
		rtIntersection winner;
		for (auto& obj : objects)
		{
			bool skip = false;
			for (auto& ex : excluded)
			{
				if (ex == obj)
				{
					skip = true;
					continue;
				}
			}

			if (skip)
				continue;

			rtIntersection is = obj->Intersect(ray);
			if (!winner.valid|| (is.valid && is.t < winner.t))
				winner = is;
		}
		return winner;
	}
	inline void Complete()
	{
		obj->CompleteIntersection(r, *this);
	}
};

struct rtSphere : public rtObject
{
	glm::vec3 pos;
	float r;
	rtSphere() = default;
	rtSphere(glm::vec3 center, float radius) : pos(center), r(radius) { }
	inline rtIntersection Intersect(const rtRay& r) const override
	{
		glm::vec3 oc = r.pos - pos;
		float fa = glm::dot(r.dir, r.dir);
		float fb = 2.f * glm::dot(r.dir, oc);
		float fc = glm::dot(oc, oc) - this->r * this->r;
		float discr = fb * fb - 4 * fa * fc;
		if (discr < 0)
		{
			// no intersection
			return rtIntersection();
		}
		else
		{
			float root1 = (float)((-fb - glm::sqrt(discr)) / (2.0f * fa));
			float root2 = (float)((-fb + glm::sqrt(discr)) / (2.0f * fa));
			float root = glm::min(root1, root2);
			if (root1 < 0.0f) root = root2;
			if (root2 < 0.0f) root = root1;
			if (root < 0.0f) return rtIntersection();
			return rtIntersection(root, this, r, true);
		}
	}
	inline void CompleteIntersection(const rtRay& ray, rtIntersection& is) const override
	{
		const rtSphere* sp = static_cast<const rtSphere*>(is.obj);
		is.p = ray.pos + ray.dir * is.t;
		is.n = glm::normalize(is.p - sp->pos);
	}
};

struct rtFramebuffer
{
	unsigned char *pixels;
	int w, h;
	GLuint texID;
	Plane plane;
	rtFramebuffer() = delete;
	rtFramebuffer(int width, int height)
	{
		w = width;
		h = height;
	}
	void Create()
	{
		glGenTextures(1, &texID);
		glBindTexture(GL_TEXTURE_2D, texID);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		plane.create(2, 2);
		plane.material.diffuse_tex = texID;
		pixels = new unsigned char[w * h * 3];
	}

	void Update()
	{
		glBindTexture(GL_TEXTURE_2D, texID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
	}
};

volatile bool active = true;
const char* title = "Realtime Raytracer 0.1";
GLFWwindow* window;
ShaderManager sm;
rtFramebuffer fb{ 512, 512 };
glm::mat4 camera = glm::translate(glm::vec3(0, 0, 10));
glm::mat4 proj = glm::ortho(-1, 1, -1, 1, -1, 1);

std::vector<const rtObject*> objects;
std::vector<const rtObject*> excluded;

struct Cursor
{
	static glm::vec2 pos;
	glm::vec2 dragStart;
	glm::vec2 dragDelta;
	bool dragging;
	bool down;
};
glm::vec2 Cursor::pos;
Cursor mouseLeft;
Cursor mouseRight;
glm::vec2 camera_rot;
glm::vec3 camera_pos;

float action_forward = 0.f;
float action_right = 0.f;

void rtTrace()
{
#ifdef RT_PARALLEL
	concurrency::parallel_for(int(0), fb.h, [&](int y)
#else
	for (int y = 0; y < fb.h; y++)
#endif
	{
		for (int x = 0; x < fb.w; x++)
		{
			// Normalized coordinate
			float u = static_cast<float>(x) / static_cast<float>(fb.w);
			float v = static_cast<float>(y) / static_cast<float>(fb.h);

			// Convert into [-1,+1] coordinate
			u = u * 2.f - 1.f;
			v = v * 2.f - 1.f;

			// Ray direction
			glm::vec3 orig{ u, v, 1.f };
			glm::vec3 dir = glm::normalize(-orig);

			// Transform to the camera
			orig = glm::vec3(camera * glm::vec4(orig, 1.f));
			dir = glm::vec3(camera * glm::vec4(dir, 0.f));

			glm::vec3 color{ dir.r * 0.5f + 0.5f };

			rtRay ray{ orig, dir };
			auto is = rtIntersection::Find(ray, objects, excluded);
			if (is.valid)
			{
				is.Complete();

				glm::vec3 reflectionColor(0);
				//// Now test for reflection
				//rtRay ray2{ is.p, is.n };
				//auto is2 = rtIntersection::Find(ray2, objects, { is.obj });
				//if (is2.valid)
				//{
				//	is2.Complete();
				//	reflectionColor = is2.n;
				//}

				// Refraction
				//rtRay ray3{ is.p + -is.n * 0.01f, -is.n };
				//auto is3 = rtIntersection::Find(ray3, { is.obj }, {});
				//if (is3.valid)
				//{
				//	is3.Complete();
				//	color = is3.n;

				//	// Ray going out of the sphere
				//	rtRay ray4{ is3.p, -is3.n };
				//	auto is4 = rtIntersection::Find(ray4, objects, { is.obj });
				//	if (is4.valid)
				//	{
				//		is4.Complete();
				//		color = is4.n;
				//	}
				//}

				{
					const glm::vec3 lightPos = glm::vec3(1.0, 1.0, -21.0);
					const glm::vec3 ambientColor = glm::vec3(0.05);
					const glm::vec3 diffuseColor = glm::vec3(0.5, 0.5, 0.5);
					const glm::vec3 specColor = glm::vec3(1.0, 1.0, 1.0) * .3f;

					glm::vec3 normal = is.n;
					glm::vec3 lightDir = normalize(lightPos - is.p);

					float lambertian = glm::max(glm::dot(lightDir, normal), 0.0f);
					float specular = 0.0;

					if (lambertian > 0.0)
					{
						glm::vec3 viewDir = glm::normalize(-is.p);
						
						// this is blinn phong
						glm::vec3 halfDir = glm::normalize(lightDir + viewDir);
						float specAngle = glm::max(dot(halfDir, normal), 0.0f);
						specular = glm::pow(specAngle, 16.0f);

						//// this is phong (for comparison)
						//if (mode == 2) 
						//{
						//	glm::vec3 reflectDir = glm::reflect(-lightDir, normal);
						//	specAngle = glm::max(glm::dot(reflectDir, viewDir), 0.0f);
						//	// note that the exponent is different here
						//	specular = glm::pow(specAngle, 4.0f);
						//}
					}

					color = glm::vec3(ambientColor +
						lambertian * diffuseColor +
						specular * specColor) + reflectionColor;
				}
			}

			color = glm::clamp(color, glm::vec3(0), glm::vec3(1));

			// Convert from linear to RGB
			int index = (y * fb.w + x) * 3;
			fb.pixels[index + 0] = static_cast<char>(color.r * 255);
			fb.pixels[index + 1] = static_cast<char>(color.g * 255);
			fb.pixels[index + 2] = static_cast<char>(color.b * 255);
		}
	}
#ifdef RT_PARALLEL
	);
#endif
}

void window_key(GLFWwindow* window,
	int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			active = false;
			break;
		case GLFW_KEY_W:
			action_forward += 1.f;
			break;
		case GLFW_KEY_S:
			action_forward -= 1.f;
			break;
		case GLFW_KEY_D:
			action_right += 1.f;
			break;
		case GLFW_KEY_A:
			action_right -= 1.f;
			break;
		default:
			break;
		}
	}
	if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_W:
			action_forward -= 1.f;
			break;
		case GLFW_KEY_S:
			action_forward += 1.f;
			break;
		case GLFW_KEY_D:
			action_right -= 1.f;
			break;
		case GLFW_KEY_A:
			action_right += 1.f;
			break;
		default:
			break;
		}
	}
}

void window_cursor(GLFWwindow* window, double x, double y)
{
	glm::vec2 oldPos(Cursor::pos);
	Cursor::pos = glm::vec2(x, y);
	mouseLeft.dragDelta = mouseLeft.down ? Cursor::pos - mouseLeft.dragStart : glm::vec2();
	mouseRight.dragDelta = mouseRight.down ? Cursor::pos - mouseRight.dragStart : glm::vec2();
	if (mouseLeft.down)
		camera_rot += glm::radians(Cursor::pos - oldPos);
}

void window_button(GLFWwindow* window, int button, int action, int mods)
{
	switch (button)
	{
	case GLFW_MOUSE_BUTTON_LEFT:
		mouseLeft.down = (action == GLFW_PRESS);
		mouseLeft.dragStart = Cursor::pos;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		mouseRight.down = (action == GLFW_PRESS);
		mouseRight.dragStart = Cursor::pos;
		break;
	}
}

int main(int argc, char** argv)
{
	glfwInit();
	window = glfwCreateWindow(fb.w, fb.h, title, nullptr, nullptr);
	glfwSetKeyCallback(window, window_key);
	glfwSetCursorPosCallback(window, window_cursor);
	glfwSetMouseButtonCallback(window, window_button);

	glfwMakeContextCurrent(window);
	glfwShowWindow(window);
	glfwSwapInterval(0);
	glewInit();

	// Init OpenGL state
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	fb.Create();
	sm.path = ".";
	sm.loadShader("color-flat");
	sm.loadShader("texture-flat");

	// Create 3D objects
	for (int i = 0; i < 30; i++)
	{
		float x = (rand() % 200 - 100) * .5f;
		float y = (rand() % 200 - 100) * .5f;
		float z = (rand() % 100 - 100) * 1.f - 20;
		rtSphere* sp = new rtSphere(glm::vec3{ x, y, z }, 5.f);
		objects.push_back(sp);
	}

	auto start = std::chrono::steady_clock::now();
	int frames = 0;
	while (active && !glfwWindowShouldClose(window))
	{
		glm::vec3 forwardVector = glm::vec3(camera * glm::vec4(0, 0, -1, 0)) * action_forward * 0.5f;
		glm::vec3 rightVector = glm::vec3(camera * glm::vec4(-1, 0, 0, 0)) * action_right * 0.5f;
		camera_pos += forwardVector + rightVector;

		camera = glm::translate(camera_pos) * glm::eulerAngleXY(camera_rot.y, camera_rot.x);

		rtTrace();
		fb.Update();

		frames++;

		auto stop = std::chrono::steady_clock::now();
		std::chrono::duration<float> diff = stop - start;
		if (diff.count() > 1)
		{
			char str[256];
			sprintf_s(str, "%s - %d fps", title, frames);
			glfwSetWindowTitle(window, str);
			printf("Tracing at %d fps\n", frames);
			frames = 0;
			start = stop;
		}

		glClearColor(.3f, .3f, .3f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		sm.useShader("texture-flat");
		sm.uniformMatrix4f("texmat", glm::mat4());
		sm.uniformMatrix4f("proj", proj);
		sm.uniformMatrix4f("modelview", fb.plane.model);
		//sm.uniform4f("color", glm::vec4(1, 0, 0, 1));
		sm.uniform1i("tex0", TEX_DIFFUSE);
		fb.plane.draw(GL_TRIANGLES);

		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	return 0;
}