#pragma once

#include <math.h>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <memory.h>
#include <cmath>

struct omiVertex
{
    glm::vec3 pos;
    glm::vec2 tex;
    glm::vec3 nor;
};

struct Material
{
    GLuint diffuse_tex;
    GLuint normal_tex;
    GLuint specular_tex;
    GLuint shadow_tex;
    
    Material();
};

struct BaseElement
{
    GLuint ibuf,vbuf;
    Material material;
    int i_count;
    int v_stride;
    long p_offset;
    long t_offset;
    long n_offset;
    long tg_offset;
    long bw_offset;
    long bid_offset;
    glm::mat4 model;
    glm::mat4 texmat;
    glm::vec4 color;

    BaseElement();
    
    virtual void draw(GLenum type) const;
};

struct Plane : public BaseElement
{
    void create(GLfloat w, GLfloat h);
};

struct Cube : public BaseElement
{
    void create(GLfloat l);
};

struct Sphere : public BaseElement
{
    void create(float radius, int divx, int divy);
    virtual void draw(GLenum type);
};

struct DistortionMesh : public BaseElement
{
    long r_offset;
    long g_offset;
    long b_offset;
    long vgn_offset;
    GLuint attVignette;
    void DistortionMesh::create(std::vector<float> vertex, std::vector<float> texR,
        std::vector<float> texG, std::vector<float> texB, std::vector<GLuint> index, std::vector<float> vignette);
    virtual void draw(GLenum type) const override;
};
