#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLEW_STATIC
#define GLM_FORCE_RADIANS
//#define GLFW_EXPOSE_NATIVE_WIN32
//#define GLFW_EXPOSE_NATIVE_WGL

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glfw3.lib")
#pragma comment(lib, "glew32s.lib")

#include <windows.h>
#include <ppl.h> // Parallel for
#include <vector>
#include <chrono>

#include <stb_image_write.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtx/transform.hpp>
#include <gtx/euler_angles.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
